var appS = angular.module('testApp.services',[]);

appS.service('copyTrans', function(){
    var transForCopy = {};
    return {
        set: function(trans){
            transForCopy = trans;
            transForCopy.amount = Math.abs(trans.amount);
        },
        get: function() {
            return transForCopy
        }
    }
})