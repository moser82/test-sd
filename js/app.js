var testApp = angular.module('testApp',['ui.router','testApp.services']);

testApp.config(function ($stateProvider,$urlRouterProvider,$locationProvider){

    $urlRouterProvider.otherwise('/login');

    $stateProvider
        .state("login",{
            url: '/login',
            templateUrl: 'views/login.html',
            controller: 'loginCtrl'
        })
        .state("signup",{
            url: '/signup',
            templateUrl: 'views/signup.html',
            controller: 'signupCtrl'
        })
        .state("account",{
            url: '/account',
            templateUrl: 'views/account.html',
            controller: 'accountCtrl'
        })
        .state("account.history",{
            url: '/history',
            templateUrl: 'views/history.html',
            controller: 'historyCtrl'
        })
        .state("account.transaction",{
            url: '/create-transaction',
            templateUrl: 'views/transaction.html',
            controller: 'transactionCtrl'
        })
    $locationProvider.html5Mode(true);
})

testApp.controller('MainCtrl',['$scope','$http',function($scope,$http){
    
}])
