var baseURL = 'http://193.124.114.46:3001/';
angular.module('testApp').controller('loginCtrl',function($scope, $http,$state){
    $scope.errorLoginForm = false;
    $scope.sendForm = function(){
        if (!$scope.email){
            alert('Please, enter e-mail');
            return;
        }
        if (!$scope.pass){
            alert('Please, enter your password');
            return;
        }
        var url = baseURL + 'sessions/create';
        var data = {
            email: $scope.email, 
            password: $scope.pass
        };

        $http.post(url,data)
            .then(function success(data){
                localStorage.setItem('id_token',data.data.id_token);
                $state.go('account.history');
            },function error(data){
                $scope.errorLoginForm = true;
                $scope.errorLoginFormText = data.data;
                //console.log(data);
            });
    }
})

angular.module('testApp').controller('signupCtrl',function($scope, $http,$state){
    $scope.errorSignupForm = false;
    $scope.sendForm = function(){
        if ($scope.pass != $scope.pass2){
            $scope.errorSignupForm = true;
            $scope.errorSignupFormText = 'Passwords are not equal';
            return;
        }
        if (!$scope.email){
            $scope.errorSignupForm = true;
            $scope.errorSignupFormText = 'Please, enter e-mail';
            return;
        }
        if (!$scope.pass){
            $scope.errorSignupForm = true;
            $scope.errorSignupFormText = 'Please, enter your password';
            return;
        }
        var url = baseURL + 'users';
        var data = {
            username: $scope.name,
            password: $scope.pass,
            email: $scope.email
        }

        $http.post(url,data)
            .then(function success(data){
                alert('Signup successfull! Now you can log in with your user\'s details');
                $state.go('login');
            },function error(data){
                $scope.errorSignupForm = true;
                $scope.errorSignupFormText = data.data;
                //console.log(data);
            });
    }    
})

angular.module('testApp').controller('accountCtrl',function($scope,$http,$state){
    var token = localStorage.getItem('id_token');
    $scope.showUserList = false;
    $scope.userNames = [];    
    if (!token){
        $state.go('login');
    }
    var header = {
        headers: {
            Authorization: 'bearer '+token,
        }
    }
    var user = {};
    $http.get(baseURL + 'api/protected/user-info',header)
        .then(function success(data){
            user = data.data.user_info_token;
            $scope.user = user;
        },function error(data){
            alert('Authorization error!');
            $state.go('login');
            console.log(data.status);
            console.log(data);
        });

    $scope.appLogOut = function(){
        $state.go('login');
        localStorage.removeItem('id_token');
    }
})

angular.module('testApp').controller('historyCtrl',function($scope,$http,$state,copyTrans){
    var token = localStorage.getItem('id_token');
    if (!token){
        $state.go('login');
    }
    var header = {
        headers: {
            Authorization: 'bearer '+token,
        }
    }
    $http.get(baseURL + 'api/protected/transactions',header)
        .then(function success(data){
            $scope.transactions = data.data.trans_token;
        },function error(data){
            alert('Authorization error!');
            $state.go('login');            
            console.log(data.status);
            console.log(data);
        })
    
    $scope.selectTransaction = function(index){
        $scope.activeRow = [];
        $scope.activeRow[index] = true;
    }

    $scope.copyTransaction = function(trans){
        copyTrans.set(trans);
        $state.go('account.transaction');
    }

})

angular.module('testApp').controller('transactionCtrl',function($scope,$http,$state,copyTrans){
    
    $scope.activeRow = [];

    var token = localStorage.getItem('id_token');
    if (!token){
        $state.go('login');
    }    
    var header = {
        headers: {
            Authorization: 'bearer '+token,
        }
    }
    
    var transForCopy = copyTrans.get();
    if (transForCopy){
        $scope.recipient = transForCopy.username;
        $scope.amount = transForCopy.amount;
    }
    
    $scope.createTransaction = function(){
        $scope.errorTransaction = false;

        var data = {
            name: $scope.recipient,
            amount: $scope.amount
        }
        $http.post(baseURL + 'api/protected/transactions',data,header)
            .then(function success(data){
                var newBalance = data.data.trans_token.balance;
                $scope.user.balance = newBalance;
                copyTrans.set({});
                alert('Transaction was completed successfully!');
                $state.go('account.history');
            },function error(data){
                $scope.errorTransaction = true;
                $scope.errorTransactionText = data.data;
                $scope.showUserList = false;
                console.log(data.status);
                console.log(data);
            })
    }

    $scope.filterUsers = function(recipient){
        $scope.errorTransaction = false;
        if (recipient.length > 2){
            $http.post(baseURL + 'api/protected/users/list',{filter: recipient},header)
                .then(function success(data){
                    $scope.userNames = data.data;
                    $scope.showUserList = true;
                },function error(data){
                    console.log(data.status);
                    console.log(data);
                })
        }
    }

    $scope.setRecipient = function(username){
        $scope.recipient = username;
        $scope.showUserList = false;
    }
})